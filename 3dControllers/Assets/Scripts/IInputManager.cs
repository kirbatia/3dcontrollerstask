
public interface IInputManager
{
    public float GetHorizontal();
    public float GetVertical();
    public float GetXRotation();
    
    public bool GetJump();
    
    public bool GetShootSmallWeapon();
    
    public bool GetShootBigWeapon();
    public bool GetFly();
}