using System.Collections.Generic;
using UnityEngine;

public class ObjectPool
{
    private  GameObject _prefab;
    private  int _poolSize;
    private  Transform _transform;
    private readonly Queue<GameObject> _objectPool = new Queue<GameObject>();

    public void Init(GameObject prefab,  int poolSize, Transform transform)
    {
        _prefab = prefab;
        _poolSize = poolSize;
        _transform = transform;
        InitializePool();
       
    }

    private void InitializePool()
    {
        for (int i = 0; i < _poolSize - 1; i++)
        {
            var obj = Object.Instantiate(_prefab, _transform);
            obj.SetActive(false);
            _objectPool.Enqueue(obj);
        }
    }

    public T GetObject<T>()
    {
        if (_objectPool.Count == 0)
        {
            var obj = Object.Instantiate(_prefab);
            obj.SetActive(false);
            _objectPool.Enqueue(obj);
        }

        var pooledObject = _objectPool.Dequeue();
        pooledObject.SetActive(true);
        var component = pooledObject.GetComponent<T>();
        return component;
    }

    public void ReturnObject(GameObject obj)
    {
        obj.SetActive(false);
        _objectPool.Enqueue(obj);
    }
}