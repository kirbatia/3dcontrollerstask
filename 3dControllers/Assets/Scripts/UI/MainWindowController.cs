using TMPro;
using UnityEngine;

public class MainWindowController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _playerAction;
    [SerializeField] private TextMeshProUGUI _energyBoost;
    [SerializeField] private TextMeshProUGUI _damageBoost;
    private PlayerController _playerController;
    private WeaponManager _weaponManager;
    private EnemyManager _enemyManager;

    public void Init(PlayerController playerController, WeaponManager weaponManager, EnemyManager enemyManager)
    {
        _playerController = playerController;
        _weaponManager = weaponManager;
        _enemyManager = enemyManager;
        
        _playerController.Moved += OnMoved;
        _playerController.Jumped += OnJumped;
        _playerController.EnergyBoosted += OnEnergyBoosted;
        _weaponManager.Fired += OnFired;
        _weaponManager.DamageBoosted += OnDamageBoosted;
        _enemyManager.Killed += OnKilled;
        
        _energyBoost.text = "Energy boost is activated";
        _damageBoost.text = "Damage boost is activated";
        _playerAction.text = "WASD - move, mouse - rotate, Z-fly, space - jump";
    }

    private void OnDamageBoosted(bool isBoosted)
    {
        _damageBoost.gameObject.SetActive(isBoosted);
    }

    private void OnEnergyBoosted(bool isBoosted)
    {
        _energyBoost.gameObject.SetActive(isBoosted);
    }

    private void OnKilled()
    {
        _playerAction.text = "Enemy is killed";
    }

    private void OnJumped()
    {
        _playerAction.text = "Player jumped";
    }

    private void OnFired(bool isBig)
    {
        var text = isBig ? "big" : "small";
        _playerAction.text = $"Player fired with {text} weapon";
    }

    private void OnMoved(bool isMoved)
    {
        var text = isMoved ? "moving" : "rotating";
        _playerAction.text = $"Player is {text}";
    }

    private void OnDisable()
    {
        _playerController.Moved -= OnMoved;
        _playerController.Jumped -= OnJumped;
        _playerController.EnergyBoosted -= OnEnergyBoosted;
        _weaponManager.Fired -= OnFired;
        _weaponManager.DamageBoosted -= OnDamageBoosted;
        _enemyManager.Killed -= OnKilled;
    }
}
