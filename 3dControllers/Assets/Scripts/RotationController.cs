using UnityEngine;

public class RotationController : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 10.0f;

    void Update()
    {
        transform.Rotate(Vector3.up * (Time.deltaTime * rotationSpeed));
    }
}