public class DamagePowerUpController : PowerUpsController
{
    protected override PowerUpType PowerUp => PowerUpType.Damage;
}