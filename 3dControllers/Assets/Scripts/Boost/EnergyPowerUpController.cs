public class EnergyPowerUpController : PowerUpsController
{
   protected override PowerUpType PowerUp => PowerUpType.Energy;
}