using System;
using UnityEngine;

public abstract class PowerUpsController : MonoBehaviour
{
    protected abstract PowerUpType PowerUp { get;  }
   
    public event Action<PowerUpType> OnPoweredUp;
    private void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<PlayerController>();
        if (player == null)
        {
            return;
        }
        gameObject.SetActive(false);
        OnPoweredUp?.Invoke(PowerUp);
    }
}