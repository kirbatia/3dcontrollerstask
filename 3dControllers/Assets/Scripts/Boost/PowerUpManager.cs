using System;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    [SerializeField] private DamagePowerUpController _damage;
    [SerializeField] private EnergyPowerUpController _energy;
    [SerializeField] private List<Transform> _damageTranforms = new List<Transform>();
    [SerializeField] private List<Transform> _energTransforms = new List<Transform>();

    private readonly List<PowerUpsController> _powerUps = new List<PowerUpsController>();
    
    public event Action EnergyPoweredUp;
    public event Action DamagedPoweredUp;

    public void Init()
    {
        foreach (var powerupTransform in _damageTranforms)
        {
            InitPowerUp(_damage, powerupTransform);
        }
        
        foreach (var powerupTransform in _energTransforms)
        {
            InitPowerUp(_energy, powerupTransform);
        }
    }

    private void InitPowerUp(PowerUpsController powerUpType, Transform transform)
    {
        var powerUp = Instantiate(powerUpType, transform) ;
        powerUp.OnPoweredUp += OnPoweredUp;
        _powerUps.Add(powerUp);
    }

    private void OnPoweredUp(PowerUpType type)
    {
        switch (type)
        {
            case PowerUpType.Energy:
                EnergyPoweredUp?.Invoke();
                break;
            case PowerUpType.Damage:
                DamagedPoweredUp?.Invoke();
                break;
        }
    }

    private void OnDisable()
    {
        foreach (var item in _powerUps)
        {
            item.OnPoweredUp -= OnPoweredUp;
        }
    }
}
