using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const float Boost = 1.5f;
    
    [SerializeField]
    private Rigidbody _rigidbody = null;

    [SerializeField] private PlayerData _playerData;
    private IInputManager _inputManager;
    private Transform _playerTransform;
    private PowerUpManager _powerUpManager;
    private Vector2 _turn;
    private float _cachedSpeed;
    private float _speed;
    private float _restriction;
    private bool _isMoving;
    private bool _isBoosted;
    private float _boostTimer = 0;
    private bool _isJumping = false;

    public event Action<bool> Moved; 
    public event Action Jumped;
    public event Action<bool> EnergyBoosted;

    public void Init(IInputManager inputManager, PowerUpManager powerUpManager)
    {
        _inputManager = inputManager;
        _powerUpManager = powerUpManager;
        _powerUpManager.EnergyPoweredUp += OnEnergized;
        _playerTransform = transform;
        _cachedSpeed = _speed = _playerData.StartSpeed;
        _restriction = _playerData.Restriction;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate()
    {
        float horizontalInput = _inputManager.GetHorizontal();
        float verticalInput = _inputManager.GetVertical();
        float mouseX = _inputManager.GetXRotation();
        if (Mathf.Abs(horizontalInput) > _restriction || Mathf.Abs(verticalInput) > _restriction)
        {
            Move(horizontalInput, verticalInput);
            if (!_isMoving)
            {
                _isMoving = true;
                Moved?.Invoke(_isMoving);
            }
        }
        else if (Math.Abs(_speed - _cachedSpeed) > _restriction)
        {
            _speed = _isBoosted ? _playerData.StartSpeed * 1.5f : _playerData.StartSpeed;
        }

        if (Mathf.Abs(mouseX) > _restriction)
        {
            Rotate(mouseX);

            if (_isMoving)
            {
                _isMoving = false;
                Moved?.Invoke(_isMoving);
            }
        }
        
        if (_inputManager.GetJump() && !_isJumping)
        {
            Jump();
            
        }

        if (_boostTimer >= 0 && _isBoosted)
        {
            _boostTimer -= Time.fixedDeltaTime;
        }
        else if(_isBoosted)
        {
            _isBoosted = false;
            EnergyBoosted?.Invoke(false);
        }
    }

    private void Move(float horizontalInput, float verticalInput)
    {
        var direction = new Vector3(horizontalInput, 0f, verticalInput);
        _playerTransform.Translate(direction * (Time.fixedDeltaTime * _speed));
        _speed += 0.05f;
    }

    private void Rotate(float input)
    {
        _turn.x += input * _playerData.Sensitivity * Time.fixedDeltaTime;
        _playerTransform.localRotation = Quaternion.Euler(0, _turn.x, 0);
    }

    private void Jump()
    {
        var force = _isBoosted ? _playerData.JumpForce * 1.5f : _playerData.JumpForce;
        _rigidbody.AddForce(Vector3.up * force, ForceMode.Impulse);
        Jumped?.Invoke();
        _isJumping = true;
    }

    private void OnEnergized()
    {
        _isBoosted = true;
        _boostTimer = 5f;
        EnergyBoosted?.Invoke(true);
    }
    
    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Ground")) 
        {
           _isJumping = false;
        }
    }
    
    private void OnDisable()
    {
        _powerUpManager.EnergyPoweredUp -= OnEnergized;
    }
}