using UnityEngine;

[CreateAssetMenu]
public class PlayerData: ScriptableObject
{
    public int StartSpeed;
    public float Restriction;
    public float Sensitivity;
    public float JumpForce;
}