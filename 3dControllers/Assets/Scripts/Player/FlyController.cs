using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FlyController : MonoBehaviour
{
    private IInputManager _inputManager;
    private Rigidbody _rb;
    private bool _isFlying;

    private void Start()
    {
        _inputManager = new InputManager();
        _rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!_inputManager.GetFly())
        {
            return;
        }
        
        _rb.useGravity = _isFlying;
        _isFlying = !_isFlying;
    }
}