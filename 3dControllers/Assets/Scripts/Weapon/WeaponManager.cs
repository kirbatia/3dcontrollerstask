using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    [SerializeField] private WeaponController _smallWeapon;
    [SerializeField] private WeaponController _bigWeapon;
    [SerializeField] private GameObject _bulletPrefab;
  
    private List<BulletController> _bullets = new List<BulletController>();
    private IInputManager _inputManager;
    private ObjectPool _pool;
    private PowerUpManager _powerUpManager;
    private bool _isBoosted;
    private float _timerBoost = 0;
    
    public event Action<Collider, float> BulletTriggered;
    public event Action<bool> Fired;
    public event Action<bool> DamageBoosted; 

    public void Init(IInputManager inputManager, PowerUpManager powerUpManager)
    {
        _inputManager = inputManager;
        _pool = new ObjectPool();
        _pool.Init(_bulletPrefab, 10, transform);
        _powerUpManager = powerUpManager;
        _powerUpManager.DamagedPoweredUp += OnDamagePoweredUp;
    }

    private void OnDamagePoweredUp()
    {
        _isBoosted = true;
        _timerBoost = 5f;
        DamageBoosted?.Invoke(true);
    }

    private void Update()
    {
        if (_inputManager.GetShootSmallWeapon())
        {
            ActivateWeapon(_smallWeapon, true);
            ActivateWeapon(_bigWeapon, false);
            Fire(_smallWeapon);
            Fired?.Invoke(false);
        }
        
        if (_inputManager.GetShootBigWeapon())
        {
            ActivateWeapon(_smallWeapon, false);
            ActivateWeapon(_bigWeapon, true);
            Fire(_bigWeapon);
            Fired?.Invoke(true);
        }

        if (_timerBoost >= 0 && _isBoosted)
        {
            _timerBoost -= Time.deltaTime;
        }
        else if(_isBoosted)
        {
            _isBoosted = false;
            DamageBoosted?.Invoke(false);
        }
    }

    private void ActivateWeapon(WeaponController weapon, bool isActive)
    {
        if (weapon.gameObject.activeSelf != isActive)
        {
            weapon.gameObject.SetActive(isActive);
        } 
    } 
    
    private void Fire(WeaponController weapon)
    {
        var bullet = _pool.GetObject<BulletController>();
        var bulletTransform = bullet.transform;
        bulletTransform.position = weapon.SpawnPosition.position;
        bulletTransform.rotation = weapon.SpawnPosition.rotation;
        bullet.Shoot(weapon.WeaponData, _isBoosted);
        bullet.Triggered += OnBulletTriggered;
        _bullets.Add(bullet);
        StartCoroutine(ReturnAfterDelay(bullet.gameObject, 3f));
    }

    private void OnBulletTriggered(Collider other, BulletController bulletController, float damage)
    {
        if (bulletController != null)
        {
            ReturnBulletToPool(bulletController.gameObject);;
        }
      
        BulletTriggered?.Invoke(other, damage);
    }

    private IEnumerator ReturnAfterDelay(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        ReturnBulletToPool(obj);
    }

    private void ReturnBulletToPool(GameObject obj)
    {
        var bullet = obj.GetComponent<BulletController>();
        bullet.Triggered -= OnBulletTriggered;
        _bullets.Remove(bullet);
        _pool.ReturnObject(obj);
    }

    private void OnDisable()
    {
        _powerUpManager.DamagedPoweredUp -= OnDamagePoweredUp;
    }
}