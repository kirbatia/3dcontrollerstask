using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private WeaponDataBase _weaponData;
    [SerializeField] private Transform _spawnPosition;

    public WeaponDataBase WeaponData => _weaponData;
    public Transform SpawnPosition => _spawnPosition;
}