using UnityEngine;

public class WeaponDataBase: ScriptableObject
{
    public int Damage;
    public float Speed;
    public float Scale;
}