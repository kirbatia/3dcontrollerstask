using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletController : MonoBehaviour
{
    private float _damage;
    private Rigidbody _rb;
   
    public event Action<Collider, BulletController, float> Triggered;

    public void Shoot(WeaponDataBase data, bool isBoosted)
    {
        var scale = data.Scale;
        _rb.transform.localScale = new Vector3(scale, scale, scale);
        _damage = isBoosted ? data.Damage* 1.5f : data.Damage;
        _rb.velocity = transform.forward * data.Speed;
    }
   
    private void Awake() 
    {
        _rb = GetComponent<Rigidbody>();
    }
   
    private void OnTriggerEnter(Collider other)
    {
        Triggered?.Invoke(other, this, _damage);
    }
}