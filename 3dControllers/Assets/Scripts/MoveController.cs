using System.Collections;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private const float Speed = 5.0f; 
    private const float Distance = 13.0f;
    private float _startPos; 
    private Transform _platformTransform;
   
    void Start()
    {
        _platformTransform = transform;
        _startPos = _platformTransform.position.x;
        StartCoroutine(MovePlatform());
    }
    
    IEnumerator MovePlatform()
    {
        while (true)
        {
            float newPos = Mathf.PingPong(Time.time * Speed, Distance) + _startPos;
            Vector3 pos = _platformTransform.position;
            pos.x = newPos;
            _platformTransform.position = pos;
            yield return null;
        }
    }
}