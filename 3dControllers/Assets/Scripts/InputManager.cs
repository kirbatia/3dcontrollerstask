using UnityEngine;

public class InputManager: IInputManager
{
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
    
    public float GetHorizontal()
    {
        return Input.GetAxis("Horizontal");
    }

    public float GetVertical()
    {
        return Input.GetAxis("Vertical");
    }

    public float GetXRotation()
    {
        return Input.GetAxis("Mouse X");
    }

    public bool GetJump()
    {
        return Input.GetKey(KeyCode.Space);
    }

    public bool GetShootSmallWeapon()
    {
        return Input.GetMouseButtonDown(0);
    }

    public bool GetShootBigWeapon()
    {
        return Input.GetMouseButtonDown(1);
    }

    public bool GetFly()
    {
        return Input.GetKey(KeyCode.Z);
    }

#endif
}