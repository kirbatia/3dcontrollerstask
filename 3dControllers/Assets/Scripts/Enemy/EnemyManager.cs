using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private BigEnemyController _bigEnemy;
    [SerializeField] private SmallEnemyController _smallEnemy;
    [SerializeField] private List<Transform> _bigEnemyTransforms = new List<Transform>();
    [SerializeField] private List<Transform> _smallEnemyTransforms = new List<Transform>();

    private readonly List<EnemyController> _enemies = new List<EnemyController>();

    public event Action Killed;

    public void Init(WeaponManager weaponManager)
    {
        foreach (var enemyTransform in _smallEnemyTransforms)
        {
            InitEnemies(_smallEnemy, enemyTransform, weaponManager);
        }
        foreach (var enemyTransform in _bigEnemyTransforms)
        {
            InitEnemies(_bigEnemy, enemyTransform, weaponManager);
        }
    }

    private void InitEnemies(EnemyController enemyType,Transform transform, WeaponManager weaponManager)
    {
        var enemy = Instantiate(enemyType, transform);
        _enemies.Add(enemy);
        enemy.Init(weaponManager);
        enemy.Killed += OnKilled;
    }

    private void OnKilled()
    {
        Killed?.Invoke();
    }

    private void OnDisable()
    {
        foreach (var enemy in _enemies)
        {
            enemy.Killed -= OnKilled;
        }
    }
}
