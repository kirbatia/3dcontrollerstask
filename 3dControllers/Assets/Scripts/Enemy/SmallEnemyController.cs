using UnityEngine;

public class SmallEnemyController : EnemyController
{
    protected override float Health { get; set; } = 200;
    
    protected override void OnBulletTriggered(Collider collider, float damage)
    {
        _enemy = collider.gameObject.GetComponent<EnemyController>() as SmallEnemyController;
        if (_enemy == null)
        {
            return;
        }
        base.OnBulletTriggered(collider, damage);
    }
}