using System.Collections;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    [SerializeField] float _speed = 1f;
    private Rigidbody _rb;
    private Vector3 _direction;

    void Start() {
        _rb = GetComponent<Rigidbody>();
        _direction = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
        StartCoroutine(UpdateDirection());
    }

    void FixedUpdate() {
        _rb.MovePosition(transform.position + _direction * _speed * Time.fixedDeltaTime);
    }

    private IEnumerator UpdateDirection() {
        while (true) {
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            _direction = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
        }
    }
}