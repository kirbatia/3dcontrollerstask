using UnityEngine;

public class BigEnemyController : EnemyController
{
    protected override float Health { get; set; } = 500;

    protected override void OnBulletTriggered(Collider collider, float damage)
    {
        _enemy = collider.gameObject.GetComponent<EnemyController>() as BigEnemyController;
        if (_enemy == null)
        {
            return;
        }
        base.OnBulletTriggered(collider, damage);
    }
}