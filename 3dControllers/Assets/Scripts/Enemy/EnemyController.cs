using System;
using UnityEngine;

public abstract class EnemyController : MonoBehaviour
{
    [SerializeField] private Material _healthy;
    [SerializeField] private Material _damaged;
    [SerializeField] private Material _critical;
    [SerializeField] private MeshRenderer _meshRenderer;
    protected abstract float Health { get; set; }
    protected EnemyController _enemy;
    private WeaponManager _weaponManager;
    private float _cachedHp;

    public event Action Killed;

    public void Init(WeaponManager weaponManager)
    {
        _cachedHp = Health;
        _weaponManager = weaponManager;
        _weaponManager.BulletTriggered += OnBulletTriggered;
        _meshRenderer.material = _healthy; 
    }

    protected virtual void OnBulletTriggered(Collider collider, float damage)
    {
        ChangeHp(damage, _enemy);
    }

    private void ChangeHp(float damage, EnemyController enemy)
    {
        Health -= damage;
        var percentage = (Health / _cachedHp) * 100;
        switch (percentage)
        {
            case <= 0:
                enemy.gameObject.SetActive(false);
                Killed?.Invoke();
                break;
            case <= 50:
                _meshRenderer.material = _critical;
                break;
            case <= 100:
                _meshRenderer.material = _damaged;
                break;
        }
    }

    private void OnDisable()
    {
        _weaponManager.BulletTriggered -= OnBulletTriggered;
    }
}