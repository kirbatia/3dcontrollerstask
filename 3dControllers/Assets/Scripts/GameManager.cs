using Unity.VisualScripting;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private EnemyManager _enemyManager;
    [SerializeField] private WeaponManager _weaponManager;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private MainWindowController _windowController;
    [SerializeField] private PowerUpManager _powerUpManager;
    private IInputManager _inputManager;

    private void Awake()
    {
        _inputManager = new InputManager();
        
        _enemyManager.Init(_weaponManager);
        _windowController.Init(_playerController, _weaponManager, _enemyManager);
        _powerUpManager.Init();
        _playerController.Init(_inputManager, _powerUpManager);
        _playerController.AddComponent<FlyController>(); // Bonus part
        _weaponManager.Init(_inputManager, _powerUpManager);
    }
}